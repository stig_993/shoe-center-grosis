var app = angular.module('grosis', [
    'ui.router',
    'rzModule',
    'uiRouterStyles',
    'cp.ngConfirm',
    'ckeditor',
    'naif.base64',
    'checklist-model',
    'reg.threesixty'
])

app.config(function ($stateProvider, $locationProvider) {
    $locationProvider.html5Mode({enabled:true,requireBase:true})

    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "view/pocetna.html",
            controller: "homeController"
        })
        .state('shop', {
            url: "/shop",
            templateUrl: "view/shop/shop.html",
            controller: "shopController"
        })
        .state('product-preview', {
            url: "/proizvod/:id",
            templateUrl: "view/shop/product.html",
            controller: "productDetailController"
        })
        .state('o-nama', {
            url: "/o-nama",
            templateUrl: "view/o-nama.html"
        })
        .state('kontakt', {
            url: "/kontakt",
            templateUrl: "view/kontakt.html",
            controller: "kontaktController"
        })

        .state('cart-preview', {
            url: "/korpa",
            templateUrl: "view/korpa.html",
            controller: "cartController"
        })

        // ADMIN ////////////////////////////////////////////
        /////////////////////////////////////////////////////
        /////////////////////////////////////////////////////

        .state('admin', {
            url: "/admin",
            templateUrl: "view/admin/login.html",
            controller: "adminController",
            data: {
                css: 'view/admin/css/admin.css'
            },
            onEnter: function ($state, token) {
                token.getToken().then(function (data) {
                    if (localStorage.getItem("token") == data) {
                        $state.go('admin');
                    }
                    if (localStorage.getItem('token') != "" && data.token == md5(localStorage.getItem('token'))) {
                        $state.go('dodaj-proizvod');
                    } else {
                        $state.go('admin');
                    }
                })
            }
        })
        .state('dodaj-proizvod', {
            url: "/dodaj-proizvod",
            templateUrl: "view/admin/dodaj-proizvod.html",
            controller: "adminController",
            data: {
                css: "view/admin/css/admin.css"
            },
            onEnter: function ($state, token) {
                token.getToken().then(function (data) {
                    if (localStorage.getItem("token") == data) {
                        $state.go('admin');
                    }
                    if (localStorage.getItem('token') != "" && data.token == md5(localStorage.getItem('token'))) {
                        $state.go('dodaj-proizvod');
                    } else {
                        $state.go('admin');
                    }
                })
            }
        })
        .state('dodaj-atribute', {
            url: "/dodaj-atribute",
            templateUrl: "view/admin/dodaj-atribute.html",
            controller: "adminController",
            data: {
                css: "view/admin/css/admin.css"
            },
            onEnter: function ($state, token) {
                token.getToken().then(function (data) {
                    if (localStorage.getItem("token") == data) {
                        $state.go('admin');
                    }
                    if (localStorage.getItem('token') != "" && data.token == md5(localStorage.getItem('token'))) {
                        $state.go('dodaj-atribute');
                    } else {
                        $state.go('admin');
                    }
                })
            }
        })
})
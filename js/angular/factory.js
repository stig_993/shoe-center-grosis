var app = angular.module('grosis');

app.factory('token', function ($http, $rootScope) {
    var token = {};

    var getToken = function() {
        return $http.get("php/auth/token.php")
            .then(function(data) {
                return data.data;
            });
    }

    return {
        getToken : getToken
    };
})
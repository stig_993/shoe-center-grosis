var app = angular.module('grosis');

app.run(function($rootScope) {
    $rootScope.cart = [];
    $rootScope.cartLength = 0;
    $rootScope.sum = 0;
    $rootScope.sumOfPrices = [];

    if (localStorage.getItem('token') == null) {
        localStorage.setItem('token', '');
    }
    // Cart
    if (localStorage.getItem('cart')) {
        var parseCart = JSON.parse(localStorage.getItem('cart'));
        $rootScope.cart = parseCart.cart;
        $rootScope.cartLength = parseCart.cart.length;
        for (var i = 0; i < $rootScope.cart.length; i++) {
            function sum(total, num) {
                return total + num;
            }

            $rootScope.sumOfPrices.push($rootScope.cart[i].cena * $rootScope.cart[i].kolicina)
            $rootScope.sum = $rootScope.sumOfPrices.reduce(sum);
        }
    }
})

app.controller('headerController', function($scope, $http, $rootScope, $state) {
    var active = true;
    $scope.showCart = function() {
        if (active) {
            TweenMax.to($('.cartPreview'), 0.5, { top: 100, alpha: 1, display: "block" });
            active = false;
        } else {
            TweenMax.to($('.cartPreview'), 0.5, { top: -220, alpha: 0, display: "none" });
            active = true;
        }
    }
    $scope.removeItem = function(index) {
        $rootScope.cart.splice(index, 1);
        $rootScope.sumOfPrices.splice(index, 1);

        $rootScope.cartLength = $rootScope.cart.length;

        function sum(total, num) {
            return total + num;
        }

        if ($rootScope.sumOfPrices.length == 0) {
            $rootScope.sumOfPrices = [];
            $rootScope.sum = 0;
        } else {
            $rootScope.sum = $rootScope.sumOfPrices.reduce(sum);
        }

        var cart = JSON.parse(localStorage.getItem('cart'));
        cart.cart = $rootScope.cart;
        var updateCart = JSON.stringify({ cart: cart.cart });
        localStorage.setItem('cart', updateCart);
    }
    if ($state.current.name == 'cart-preview') {
        $scope.hideCart = true;
    }
    $scope.burger = function() {
        if (active) {
            TweenMax.to($('.burger2'), 0.25, { opacity: 0 });
            TweenMax.to($('.burger1'), 0.25, { top: "8px", rotation: 45 });
            TweenMax.to($('.burger3'), 0.25, { rotation: -45, top: "8px" });
            TweenMax.to($('.mobileMenu'), 0.25, { top: 0 });
            $('body').css('overflow-y', 'hidden');
            active = false;
        } else {
            TweenMax.to($('.burger2'), 0.25, { opacity: 1 });
            TweenMax.to($('.burger1'), 0.25, { top: "0px", rotation: 0 });
            TweenMax.to($('.burger3'), 0.25, { rotation: 0, top: "16px" });
            TweenMax.to($('.mobileMenu'), 0.25, { top: "-100%" });
            $('body').css('overflow-y', 'scroll');
            active = true;
        }
    }

    $('.mobileMenu ul li').on('click', function() {
        TweenMax.to($('.burger2'), 0.25, { opacity: 1 });
        TweenMax.to($('.burger1'), 0.25, { top: "0px", rotation: 0 });
        TweenMax.to($('.burger3'), 0.25, { rotation: 0, top: "16px" });
        TweenMax.to($('.mobileMenu'), 0.25, { top: "-100%" });
        $('body').css('overflow-y', 'scroll');
        active = true;
    })

    $scope.viewCart = function() {
        if ($rootScope.cartLength != 0) {
            $state.go('cart-preview');
        }
    }
})

app.controller('homeController', function($scope, $http, $timeout) {
    VanillaTilt.init(document.querySelector(".tilt"), {
        max: 25,
        speed: 400,
        reverse: true,
        perspective: 1000
    });

    // Pocetna strana proizvodi
    $scope.homeLoading = false;
    $http({
        method: "GET",
        url: "php/proizvodi/prikazi-home-proizvode.php"
    }).then(function(res) {
        $scope.proizvodi = res.data;
        if ($scope.proizvodi[0].proizvod_id == undefined) {
            $scope.empty = true;
        } else {
            $scope.empty = false;
        }
        $scope.homeLoading = true;
    })

    // Owl carousel
    $timeout(function() {
        $(".owl-carousel").owlCarousel({
            nav: true,
            rewind: true,
            autoplay: true,
            autoplayTimeout: 5000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });
        $('.owl-prev').html("<img src='img/icons/back.png'>");
        $('.owl-next').html("<img src='img/icons/next.png'>");
    }, 500);
})

app.controller('shopController', function($scope, $http, $state, $rootScope, $timeout, $filter) {
    // Svi proizvodi
    $scope.prikaziSveProizvode = function() {
        $http({
            method: "GET",
            url: "php/proizvodi/prikazi-proizvode.php"
        }).then(function() {})
    }
    $scope.prikaziSveProizvode();

    $scope.reload = function() {
        $state.reload();
    }

    $scope.showSearch = function() {
        TweenMax.to($('.searchDiv'), 0.5, { alpha: 1, scale: 1 });
        $('.searchDiv input').focus();
        $('body').css('overflow-y', 'hidden')
        $('.closeBtn').click(function() {
            TweenMax.to($('.searchDiv'), 0.5, { alpha: 0, scale: 0 });
            $('body').css('overflow-y', 'scroll');
        })
        $('.searchBtn').click(function() {
            TweenMax.to($('.searchDiv'), 0.5, { alpha: 0, scale: 0 })
            $('body').css('overflow-y', 'scroll')
        })
    }

    $scope.search = {
        txt: ""
    }
    $scope.searchForm = function() {
        $http({
            method: "POST",
            data: $scope.search,
            url: "php/filter/search.php"
        }).then(function(res) {
            $scope.proizvodi = res.data;
            $scope.search.txt = "";
            if (res.data == '') {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
        })
    }
    $scope.slider = {
        min: 10,
        max: 30000,
        options: {
            floor: 10,
            ceil: 30000,
            pushRange: true,
            step: 10,
        }
    };
    var min;
    var max;

    // Proizvodi za shop stranu
    $scope.loading = false;
    $scope.prikaziShopProizvode = function() {
        $http({
            method: "GET",
            url: "php/proizvodi/prikazi-shop-proizvode.php"
        }).then(function(res) {
            $scope.proizvodi = res.data;
            $rootScope.products = res.data;
            $scope.loading = true;
        })
    }
    $scope.prikaziShopProizvode();

    $scope.$on("slideEnded", function() {
        min = $scope.slider.min;
        max = $scope.slider.max;
        $scope.filter();
    });

    $scope.filterProduct = {
        kategorije: [],
        brendovi: [],
        promocije: [],
        mz: [],
        velicine: [],
        raspon_cene: {
            min: $scope.slider.min,
            max: $scope.slider.max
        }
    }

    // Prikazi atribute
    $scope.prikaziBrendove = function() {
        $http({
            method: "GET",
            url: "php/atributi/brendovi/prikazi-brendove.php"
        }).then(function(res) {
            $scope.brendovi = res.data;
            console.log(res.data);
        })
    }
    $scope.prikaziBrendove();
    $scope.prikaziKategorije = function() {
        $http({
            method: "GET",
            url: "php/atributi/kategorije/prikazi-kat.php"
        }).then(function(res) {
            $scope.kategorije = res.data;
        })
    }
    $scope.prikaziKategorije();
    $scope.prikaziPromocije = function() {
        $http({
            method: "GET",
            url: "php/atributi/promocije/prikazi-pro.php"
        }).then(function(res) {
            $scope.promocije = res.data;
        })
    }
    $scope.prikaziPromocije();
    $scope.prikaziVelicine = function() {
        $http({
            method: "GET",
            url: "php/atributi/velicine/prikazi-vel.php"
        }).then(function(res) {
            $scope.velicine = res.data;
        })
    }
    $scope.prikaziVelicine();
    ///

    $scope.mz = [
        { value: "1", naziv: "Muške" },
        { value: "2", naziv: "Ženske" }
    ]

    $scope.sort = [
        { type: 'Rastuće', text: 'Najjeftinije prvo' },
        { type: 'Opadajuće', text: 'Najskuplje prvo' }
    ]

    $scope.filter = function() {
        var brend;
        var kategorija;
        var promocija;
        var velicina;
        var gender;

        brend = $scope.filterProduct.brendovi.filter(function(element) {
            return element !== undefined;
        });
        kategorija = $scope.filterProduct.kategorije.filter(function(element) {
            return element !== undefined;
        });
        promocija = $scope.filterProduct.promocije.filter(function(element) {
            return element !== undefined;
        });
        velicina = $scope.filterProduct.velicine.filter(function(element) {
            return element !== undefined;
        });
        gender = $scope.filterProduct.mz.filter(function(element) {
            return element !== undefined;
        });



        filter = {
            brendovi: brend,
            kategorije: kategorija,
            promocije: promocija,
            velicine: velicina,
            pol: gender,
            raspon_cene: [
                { minPrice: min },
                { maxPrice: max }
            ],
            type: $scope.sort.type
        }


        $http({
            method: "POST",
            data: filter,
            url: "php/filter/prikazi-rezultate.php"
        }).then(function(res) {
            $scope.proizvodi = res.data;
            // Prikazivanje poruke ako nema rezultata
            if (res.data.empty) {
                $scope.empty = true;
            } else {
                $scope.empty = false;
            }
            if (res.data[0] == null) {
                $scope.proizvodi = $rootScope.products;
            }
        })
    }

})

app.controller('productDetailController', function($scope, $rootScope, $http, $stateParams, $sce, $ngConfirm) {
    // $("#mainProduct").elevateZoom();

    // Proizvod

    $scope.loading = false;
    $http({
        method: "POST",
        data: { id: $stateParams.id },
        url: "php/proizvodi/prikazi-proizvod.php"
    }).then(function(res) {
        $scope.proizvod = res.data;
        $scope.loading = true;
        $scope.proizvod.proizvod.duzi_opis = $sce.trustAsHtml($scope.proizvod.proizvod.duzi_opis);
        $scope.imageList = [];
        if (res.data.panSlike[0] != null) {
            for (var i = 0; i < res.data.panSlike.length; i++) {
                var list = res.data.panSlike[i].slika;
                $scope.imageList.push('uploads/proizvodi/360/' + list);
            }
        } else {
            $scope.imageList = '';
        }
    })

    $scope.viewPan = function() {
        $('.modalDiv').css('display', 'block');
        $('.hideDiv').css('display', 'none');
    }

    //Slike
    $http({
        method: "POST",
        data: { id: $stateParams.id },
        url: "php/proizvodi/prikazi-slike.php"
    }).then(function(res) {
        $scope.slike = res.data;
    })

    $scope.quant = 1;
    $scope.increment = function() {
        $scope.quant++;
    }
    $scope.decrement = function() {
        if ($scope.quant > 1) {
            $scope.quant--;
        }
    }

    $scope.izabraneVelicine = {
        broj: ""
    }
    $scope.addSize = function() {
        $scope.size = $scope.izabraneVelicine.broj;
    }

    $scope.$watch('izabraneVelicine.broj', function() {
        $scope.error = false;
    })
    $scope.addToCart = function(naziv, slika, cena, sifra) {
        if ($scope.size != undefined && $scope.size != '') {
            var pushed;
            for (var i = 0; i < $rootScope.cart.length; i++) {
                if ($rootScope.cart[i].naziv == naziv) {
                    $rootScope.cart[i].cena == parseInt(cena);
                    pushed = true;
                }
            }
            if (pushed == undefined) {
                $rootScope.cart.push({
                    "cena": parseInt(cena),
                    "naziv": naziv,
                    "slika": slika,
                    "sifra": sifra,
                    "kolicina": $scope.quant,
                    "velicina": $scope.size
                });
                var str = JSON.stringify({ "cart": $rootScope.cart });
                localStorage.setItem("cart", str);

                function sum(num1, num2) {
                    return num1 + num2;
                }

                $rootScope.sumOfPrices.push(parseInt(cena) * $scope.quant);
                $rootScope.cartLength = $rootScope.cart.length;
                $rootScope.sum = $rootScope.sumOfPrices.reduce(sum);
            }
            $scope.error = false;
        } else {
            $scope.error = true;
            $scope.msg = "Morate izabrati velicinu cipele.";
        }
    }

    $("[data-fancybox]").fancybox({
        thumbs: {
            autoStart: true
        }
    });
})

app.controller('cartController', function($scope, $rootScope, $timeout, $http, $ngConfirm) {
    $scope.remove = function(index) {
        $ngConfirm({
            title: "Brisanje proizvoda iz korpe",
            content: "Da li ste sigurni da želite da izbacite ovaj proizvod iz korpe?",
            type: 'black',
            typeAnimated: true,
            buttons: {
                Da: {
                    text: 'Da',
                    btnClass: 'btn-red',
                    action: function() {
                        $timeout(function() {
                            $rootScope.cart.splice(index, 1);
                            $rootScope.sumOfPrices.splice(index, 1);
                            $rootScope.cartLength = $rootScope.cart.length;

                            function sum(total, num) {
                                return total + num;
                            }

                            if ($rootScope.sumOfPrices.length == 0) {
                                $rootScope.sumOfPrices = [];
                                $rootScope.sum = 0;
                            } else {
                                $rootScope.sum = $rootScope.sumOfPrices.reduce(sum);
                            }
                            var cart = JSON.parse(localStorage.getItem('cart'));
                            cart.cart = $rootScope.cart;
                            var updateCart = JSON.stringify({ cart: cart.cart });
                            localStorage.setItem('cart', updateCart);
                        }, 200)
                    }
                },
                Ne: {
                    text: "Ne",
                    btnClass: 'btn-blue',
                    action: function() {}
                }
            }
        })
    }
    $scope.increment = function(index) {
        $rootScope.cart[index].kolicina++;
        $rootScope.cart[index].cena * $rootScope.cart[index].kolicina;

        function sum(num1, num2) {
            return num1 + num2;
        }

        $rootScope.sumOfPrices[index] = $rootScope.cart[index].cena * $rootScope.cart[index].kolicina;
        $rootScope.sum = $rootScope.sumOfPrices.reduce(sum);
    }
    $scope.decrement = function(index) {
        if ($rootScope.cart[index].kolicina > 1) {
            $rootScope.cart[index].kolicina--;

            function sum(num1, num2) {
                return num1 + num2;
            }

            $rootScope.sumOfPrices[index] = $rootScope.cart[index].cena * $rootScope.cart[index].kolicina;
            $rootScope.sum = $rootScope.sumOfPrices.reduce(sum);
        }
    }
    $scope.nextStep = false;
    $scope.confirm = function() {
        $scope.nextStep = true;
    }

    $scope.info = {
        ime: "",
        telefon: "",
        email: "",
        adresa: "",
        grad: "",
        code: "",
    }
    $scope.finished = false;
    $scope.finish = function(valid) {
        var str = JSON.stringify({ info: $scope.info, cart: $scope.cart, sum: $scope.sum });
        if (valid) {
            $http({
                method: "POST",
                data: str,
                url: "php/email/email.php"
            }).then(function() {
                $scope.finished = true;
            })
        }
    }
})

app.controller('adminController', function($scope, $rootScope, $http, $state, $ngConfirm) {
    //Auth
    $scope.login = {
        username: "",
        password: ""
    }
    $scope.loginAdmin = function() {
        var encode = btoa($scope.login.username + ":" + $scope.login.password);
        $http({
            headers: {
                'Authorization': 'Basic ' + encode
            },
            method: "POST",
            url: "php/auth/admin.php"
        }).then(function(data) {
            $scope.message = data.data;
            if (data.data.loggedIn) {
                $rootScope.loggedIn = true;
                localStorage.setItem('token', data.data.token);
                $state.go('dodaj-proizvod');
            } else {
                $scope.loggedFalse = true;
            }
        })
    }
    $scope.logout = function() {
        localStorage.setItem('token', '')
        $state.reload();
    }
    $scope.reload = function() {
        $state.reload();
    }
    // Atributi /////////////////
    if ($state.current.name == 'dodaj-atribute') {

        // Brendovi /////////////////////////////////////////////////////////
        $scope.brend = {
            naziv: ""
        }
        $scope.brandPost = true;
        $scope.brandPut = false;
        $scope.prikaziBrendove = function() {
            $http({
                method: "GET",
                url: "php/atributi/brendovi/prikazi-brendove.php"
            }).then(function(res) {
                $scope.brendovi = res.data;
            })
        }
        $scope.prikaziBrendove();
        $scope.deleteBrand = function(id) {
            $ngConfirm({
                title: "Brisanje brenda",
                content: "Da li ste sigurni da želite da obrišete ovaj brend?",
                type: 'red',
                typeAnimated: true,
                buttons: {
                    Da: {
                        text: 'Da',
                        btnClass: 'btn-red',
                        action: function(scope, button) {
                            $http({
                                method: "DELETE",
                                data: { id: id },
                                url: "php/atributi/brendovi/izbrisi-brend.php"
                            }).then(function() {
                                $scope.prikaziBrendove();
                            })
                        }
                    },
                    Ne: {
                        text: "Ne",
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                }
            })
        }
        $scope.editBrand = function(id) {
            $scope.brandPost = false;
            $scope.brandPut = true;
            $scope.brandId = id;
            $http({
                method: "POST",
                data: { id: id },
                url: "php/atributi/brendovi/edit-brend.php"
            }).then(function(res) {
                $scope.brend = {
                    naziv: res.data.naziv_brenda
                }
            })
        }
        $scope.dodajBrend = function() {
            if ($scope.brandPost) {
                $http({
                    method: "POST",
                    url: "php/atributi/brendovi/dodaj-brend.php",
                    data: $scope.brend
                }).then(function() {
                    $scope.prikaziBrendove();
                    $state.reload();
                })
            } else {
                $http({
                    method: "PUT",
                    url: "php/atributi/brendovi/izmeni-brend.php",
                    data: {
                        naziv: $scope.brend.naziv,
                        id: $scope.brandId
                    }
                }).then(function() {
                    $scope.prikaziBrendove();
                    $state.reload();
                })
            }
        }
        ////////////////////////////////////////////////////////////////////////

        // Kategorije /////////////////////////////////////////////////////////
        $scope.kategorija = {
            naziv: ""
        }
        $scope.katPost = true;
        $scope.katPut = false;
        $scope.prikaziKategorije = function() {
            $http({
                method: "GET",
                url: "php/atributi/kategorije/prikazi-kat.php"
            }).then(function(res) {
                $scope.kategorije = res.data;
            })
        }
        $scope.prikaziKategorije();
        $scope.deleteKat = function(id) {
            $ngConfirm({
                title: "Brisanje kategorije",
                content: "Da li ste sigurni da želite da obrišete ovu kategoriju?",
                type: 'red',
                typeAnimated: true,
                buttons: {
                    Da: {
                        text: 'Da',
                        btnClass: 'btn-red',
                        action: function(scope, button) {
                            $http({
                                method: "DELETE",
                                data: { id: id },
                                url: "php/atributi/kategorije/izbrisi-kat.php"
                            }).then(function() {
                                $scope.prikaziKategorije();
                            })
                        }
                    },
                    Ne: {
                        text: "Ne",
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                }
            })
        }
        $scope.editKat = function(id) {
            $scope.katPost = false;
            $scope.katPut = true;
            $scope.kategorijaId = id;
            $http({
                method: "POST",
                data: { id: id },
                url: "php/atributi/kategorije/edit-kat.php"
            }).then(function(res) {
                $scope.kategorija = {
                    naziv: res.data.naziv_kategorije
                }
            })
        }
        $scope.dodajKategoriju = function() {
            if ($scope.katPost) {
                $http({
                    method: "POST",
                    url: "php/atributi/kategorije/dodaj-kat.php",
                    data: $scope.kategorija
                }).then(function() {
                    $scope.prikaziKategorije();
                    $state.reload();
                })
            } else {
                $http({
                    method: "PUT",
                    url: "php/atributi/kategorije/izmeni-kat.php",
                    data: {
                        naziv: $scope.kategorija.naziv,
                        id: $scope.kategorijaId
                    }
                }).then(function() {
                    $scope.prikaziKategorije();
                    $state.reload();
                })
            }
        }
        ////////////////////////////////////////////////////////////////////////

        // Promocije /////////////////////////////////////////////////////////
        $scope.promocija = {
            naziv: ""
        }
        $scope.proPost = true;
        $scope.proPut = false;
        $scope.prikaziPromocije = function() {
            $http({
                method: "GET",
                url: "php/atributi/promocije/prikazi-pro.php"
            }).then(function(res) {
                $scope.promocije = res.data;
            })
        }
        $scope.prikaziPromocije();
        $scope.deletePro = function(id) {
            $ngConfirm({
                title: "Brisanje promocije",
                content: "Da li ste sigurni da želite da obrišete ovu promociju?",
                type: 'red',
                typeAnimated: true,
                buttons: {
                    Da: {
                        text: 'Da',
                        btnClass: 'btn-red',
                        action: function(scope, button) {
                            $http({
                                method: "DELETE",
                                data: { id: id },
                                url: "php/atributi/promocije/izbrisi-pro.php"
                            }).then(function() {
                                $scope.prikaziPromocije();
                            })
                        }
                    },
                    Ne: {
                        text: "Ne",
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                }
            })
        }
        $scope.editPro = function(id) {
            $scope.proPost = false;
            $scope.proPut = true;
            $scope.promocijaId = id;
            $http({
                method: "POST",
                data: { id: id },
                url: "php/atributi/promocije/edit-pro.php"
            }).then(function(res) {
                $scope.promocija = {
                    naziv: res.data.vrsta_promocije
                }
            })
        }
        $scope.dodajPromociju = function() {
            if ($scope.proPost) {
                $http({
                    method: "POST",
                    url: "php/atributi/promocije/dodaj-pro.php",
                    data: $scope.promocija
                }).then(function() {
                    $scope.prikaziPromocije();
                    $state.reload();
                })
            } else {
                $http({
                    method: "PUT",
                    url: "php/atributi/promocije/izmeni-pro.php",
                    data: {
                        naziv: $scope.promocija.naziv,
                        id: $scope.promocijaId
                    }
                }).then(function() {
                    $scope.prikaziPromocije();
                    $state.reload();
                })
            }
        }
        ////////////////////////////////////////////////////////////////////////

        // Velicine /////////////////////////////////////////////////////////
        $scope.velicina = {
            broj: ""
        }
        $scope.velPost = true;
        $scope.velPut = false;
        $scope.prikaziVelicine = function() {
            $http({
                method: "GET",
                url: "php/atributi/velicine/prikazi-vel.php"
            }).then(function(res) {
                $scope.velicine = res.data;
            })
        }
        $scope.prikaziVelicine();
        $scope.deleteVel = function(id) {
            $ngConfirm({
                title: "Brisanje promocije",
                content: "Da li ste sigurni da želite da obrišete ovu velicinu?",
                type: 'red',
                typeAnimated: true,
                buttons: {
                    Da: {
                        text: 'Da',
                        btnClass: 'btn-red',
                        action: function(scope, button) {
                            $http({
                                method: "DELETE",
                                data: { id: id },
                                url: "php/atributi/velicine/izbrisi-vel.php"
                            }).then(function() {
                                $scope.prikaziVelicine();
                            })
                        }
                    },
                    Ne: {
                        text: "Ne",
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                }
            })
        }
        $scope.editVel = function(id) {
            $scope.velPost = false;
            $scope.velPut = true;
            $scope.velicinaId = id;
            $http({
                method: "POST",
                data: { id: id },
                url: "php/atributi/velicine/edit-vel.php"
            }).then(function(res) {
                $scope.velicina = {
                    broj: res.data.broj
                }
            })
        }
        $scope.dodajVelicinu = function() {
            if ($scope.velPost) {
                $http({
                    method: "POST",
                    url: "php/atributi/velicine/dodaj-vel.php",
                    data: $scope.velicina
                }).then(function() {
                    $scope.prikaziVelicine();
                    // $state.reload();
                })
            } else {
                $http({
                    method: "PUT",
                    url: "php/atributi/velicine/izmeni-vel.php",
                    data: {
                        broj: $scope.velicina.broj,
                        id: $scope.velicinaId
                    }
                }).then(function() {
                    $scope.prikaziVelicine();
                    $state.reload();
                })
            }
        }
        ////////////////////////////////////////////////////////////////////////
    }

    if ($state.current.name == 'dodaj-proizvod') {
        $scope.onReady = function() {};
        $scope.prikaziBrendove = function() {
            $http({
                method: "GET",
                url: "php/atributi/brendovi/prikazi-brendove.php"
            }).then(function(res) {
                $scope.brendovi = res.data;
            })
        }
        $scope.prikaziBrendove();
        $scope.prikaziPromocije = function() {
            $http({
                method: "GET",
                url: "php/atributi/promocije/prikazi-pro.php"
            }).then(function(res) {
                $scope.promocije = res.data;
            })
        }
        $scope.prikaziPromocije();
        $scope.prikaziKategorije = function() {
            $http({
                method: "GET",
                url: "php/atributi/kategorije/prikazi-kat.php"
            }).then(function(res) {
                $scope.kategorije = res.data;
            })
        }
        $scope.prikaziKategorije();
        $scope.prikaziVelicine = function() {
            $http({
                method: "GET",
                url: "php/atributi/velicine/prikazi-vel.php"
            }).then(function(res) {
                $scope.velicine = res.data;
            })
        }
        $scope.prikaziVelicine();

        // Dodavanje proizvoda //////////////////////////////////
        $scope.mzc = [
            { value: "Muska" },
            { value: "Zenska" }
        ]

        $scope.proizvod = {
            naziv: "",
            brend: "",
            kategorija: "",
            sifra: "",
            cena: "",
            stara_cena: "",
            kratak_opis: "",
            duzi_opis: "",
            dostupno: "",
            shop_strana: "",
            home_strana: "",
            promocija: "",
            mz: "",
            velicine: []
        }
        $scope.productImages = {
            image: ""
        }

        $scope.panoramaImages = {
            image: ""
        }

        $scope.images = false;
        $scope.nextStep = function() {
            $scope.images = true;
        }

        $scope.prikaziProizvode = function() {
            $http({
                method: "GET",
                url: "php/proizvodi/prikazi-proizvode.php"
            }).then(function(res) {
                $scope.proizvodi = res.data;
            })
        }
        $scope.prikaziProizvode();

        $scope.$watch('productImages.image', function(res) {
            $scope.imagePreview = res;
        })
        $scope.$watch('panoramaImages.image', function(res) {
            $scope.panImagePrev = res;
        })
        $scope.deleteProizvod = function(id) {
            $ngConfirm({
                title: "Brisanje proizvoda",
                content: "Da li ste sigurni da želite da obrišete ovaj proizvod?",
                type: 'red',
                typeAnimated: true,
                buttons: {
                    Da: {
                        text: 'Da',
                        btnClass: 'btn-red',
                        action: function(scope, button) {
                            $http({
                                method: "DELETE",
                                data: { id: id },
                                url: "php/proizvodi/izbrisi-proizvod.php"
                            }).then(function() {
                                $scope.prikaziProizvode();
                            })
                        }
                    },
                    Ne: {
                        text: "Ne",
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                }
            })
        }
        /////////////////////////////////////////////////////////

        // Editovanje proizvoda /////////////////////////////////
        $scope.editStatus = false;
        $scope.proizvodPost = true;
        $scope.proizvodPut = false;
        $scope.noImages = false;
        $scope.editProizvod = function(id) {
            $scope.editStatus = true;
            $scope.proizvodPost = false;
            $scope.proizvodPut = true;
            $scope.proizvodId = id;
            $http({
                method: "POST",
                data: { id: id },
                url: "php/proizvodi/edit-proizvod.php"
            }).then(function(res) {
                $scope.slike = res.data.slike;

                if (res.data.slike[0] == null) {
                    $scope.noImages = true;
                }

                $scope.panSlike = res.data.panSlike;
                $scope.panSlikeLength = res.data.panSlike.length;
                $scope.noPanImages = false;

                if (res.data.panSlike[0] == null) {
                    $scope.noPanImages = true;
                }

                if (res.data.proizvod.dostupnost == '1') {
                    res.data.proizvod.dostupnost = true;
                }
                if (res.data.proizvod.shop_strana == '1') {
                    res.data.proizvod.shop_strana = true;
                }
                if (res.data.proizvod.home_strana == '1') {
                    res.data.proizvod.home_strana = true;
                }

                if (res.data.brend == null) {
                    res.data.brend = '0';
                }

                if (res.data.proizvod.mz == "1") {
                    res.data.proizvod.mz = 'Muska';
                } else if (res.data.proizvod.mz == '2') {
                    res.data.proizvod.mz = 'Zenska';
                } else {
                    res.data.proizvod.mz = '0';
                }
                $scope.pickedSizes = res.data.velicine;
                $scope.proizvod = {
                    naziv: res.data.proizvod.naziv,
                    brend: res.data.brend.brend_id,
                    sifra: res.data.proizvod.sifra,
                    cena: res.data.proizvod.cena,
                    stara_cena: res.data.proizvod.stara_cena,
                    kratak_opis: res.data.proizvod.kratak_opis,
                    duzi_opis: res.data.proizvod.duzi_opis,
                    dostupno: res.data.proizvod.dostupnost,
                    shop_strana: res.data.proizvod.shop_strana,
                    home_strana: res.data.proizvod.home_strana,
                    mz: res.data.proizvod.mz,
                    promocija: res.data.proizvod.fk_promocija_id,
                    kategorija: res.data.proizvod.fk_kategorija_id,
                    velicine: []
                }
            })
        }

        $scope.panRemoveImages = false;
        $scope.removePanImages = function() {
            $scope.panRemoveImages = true;
        };

        $scope.removedSize = [];
        $scope.removeSize = function(index, proizvod, velicina) {
            $scope.removedSize.push({ proizvod_id: proizvod, velicina_id: velicina });
            $scope.pickedSizes.splice(index, 1);
        }

        $scope.imagesToRemove = [];
        $scope.removeImage = function(index, id, slika) {
            $scope.imagesToRemove.push({ id: id, slika: slika });
            $scope.slike.splice(index, 1);
        }

        $scope.dodajProizvod = function(valid) {
            var velicine;
            velicine = $scope.proizvod.velicine.filter(function(element) {
                return element !== undefined;
            });
            $scope.proizvod.velicine = velicine;
            $('.loader').css('display', 'flex');
            $('body').css('overflow-y', 'hidden');
            if ($scope.proizvod.mz == "Muska") {
                $scope.proizvod.mz = '1';
            } else if ($scope.proizvod.mz == "Zenska") {
                $scope.proizvod.mz = '2';
            } else {
                $scope.proizvod.mz = '0';
            }
            if (valid) {
                if ($scope.proizvodPost) {
                    $http({
                        method: "POST",
                        data: {
                            product: $scope.proizvod,
                            images: $scope.productImages,
                            panorama: $scope.panoramaImages
                        },
                        url: "php/proizvodi/dodaj-proizvod.php"
                    }).then(function() {
                        $state.reload();
                        $('.loader').css('display', 'none');
                        $('body').css('overflow-y', 'scroll');
                    })
                } else {
                    if ($scope.removedSize == undefined) {
                        $scope.removedSize = "";
                    }

                    $http({
                        method: "PUT",
                        data: {
                            product: $scope.proizvod,
                            id: $scope.proizvodId,
                            removedImages: $scope.imagesToRemove,
                            removedSize: $scope.removedSize,
                            images: $scope.productImages,
                            removePanImgs: $scope.panRemoveImages,
                            panorama: $scope.panoramaImages
                        },
                        url: "php/proizvodi/azuriraj-proizvod.php"
                    }).then(function() {
                        $state.reload();
                        $('.loader').css('display', 'none');
                        $('body').css('overflow-y', 'scroll');
                    })
                }
            }
        }
    }
})

app.controller('kontaktController', function($scope, $http) {
    $scope.kontakt = {
        ime: "",
        email: "",
        poruka: ""
    }
    $scope.send = function() {
        $http({
            method: "POST",
            data: $scope.kontakt,
            url: "php/kontakt.php"
        }).then(function() {
            $scope.msg = "Uspešno ste poslali poruku."
        })
    }
})

app.controller('footerController', function($scope) {
    $scope.year = new Date();
})
<?php

include_once '../mysql.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn, "utf8");
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

////////////

if (!empty($request->brendovi)) {
	$brendovi = array();

	foreach ($request->brendovi as $b) {
		$brendovi[] = "$b";
		$brends = array_filter($brendovi);
	}

	$brends = implode(", ", $brends);
	$getBrends = "IN ($brends)";

	// Check if is empty filtered array
	if (empty($brends)) {
		$brends = "''";
		$getBrends = "";
	}

} else {
	$brends = "''";
	$getBrends = "";	
}

if (!empty($request->kategorije)) {
	$kategorije = array();

	foreach ($request->kategorije as $k) {
		$kategorije[] = "$k";
		$categories = array_filter($kategorije);
	}

	$categories = implode(", ", $categories);
	$getCats = "AND `fk_kategorija_id` IN ($categories)";

	// Check if is empty filtered array
	if (empty($categories)) {
		$categories = "''";
		$getCats = "";
	}
} else {
	$categories = "''";
	$getCats = "";
}

if (!empty($request->promocije)) {
	$promocije = array();

	foreach ($request->promocije as $p) {
		$promocije[] = "$p";
		$promotions = array_filter($promocije);
	}

	$promotions = implode(", ", $promotions);

	$getPromotions = "AND `fk_promocija_id` IN ($promotions)";

	// Check if is empty filtered array
	if (empty($promotions)) {
		$promotions = "''";
		$getPromotions = "";
	}
} else {
	$promotions = "''";
	$getPromotions = "";
}

if (!empty($request->pol)) {
	$genderArray = array();

	foreach ($request->pol as $p) {
		$genderArray[] = "$p";
		$mz = array_filter($genderArray);
	}

	$mz = implode(", ", $mz);

	$getGender = "AND `mz` IN ($mz)";

	// Check if is empty filtered array
	if (empty($mz)) {
		$mz = "''";
		$getGender = "";
	}
} else {
	$mz = "''";
	$getGender = "";
}

if (!empty($request->velicine)) {
	$velicine = array();

	foreach ($request->velicine as $v) {
		$velicine[] = "$v";
		$sizes = array_filter($velicine);
	}

	$isBool = false;
	for($t = 0; $t < count($request->velicine); $t++) {
		if (!is_bool($request->velicine[$t])) {
			$isBool = true;
		}
	}

	if ($isBool) {
		$sizes = implode(", ", $sizes);

		$vSql = "SELECT * FROM `proizvod-velicina` WHERE fk_velicina_id IN ($sizes)";

		$vresult = $conn->query($vSql);

		$size = array();

		if ($vresult->num_rows > 0) {
			while($row = $vresult->fetch_object()) {
				$size[] = $row;
			}
		} else {
			$size[] = null;
		}

		$proizvodi = array();

		if (!empty($size[0])) {
			for ($p = 0; $p < count($size); $p++) {
				$proizvodi[] = $size[$p]->fk_proizvod_id;
				$filteredProizvodi = array_unique($proizvodi);
			}

			$sizes = implode(", ", $filteredProizvodi);

			$getProizvod = "AND `proizvod_id` IN ($sizes)";
		} else {
			echo json_encode(array("empty" => true));
			die();
		}
	}

	// Check if is empty filtered array
	if (empty($sizes)) {
		$sizes = "''";
		$getProizvod = "";
	}
} else {
	$sizes = "''";
	$getProizvod = "";
}

////////////

if (empty($request->raspon_cene[0]->minPrice)) {
	$minPrice = "0";

} else {
	$minPrice = $request->raspon_cene[0]->minPrice;
}

if (empty($request->raspon_cene[1]->maxPrice)) {
	$maxPrice = "''";
} else {
	$maxPrice = $request->raspon_cene[1]->maxPrice;
}

if (!empty($request->raspon_cene[0]->minPrice) && !empty($request->raspon_cene[1]->maxPrice)) {
	$getPrice = "AND `cena` BETWEEN $minPrice AND $maxPrice";
} else {
	$getPrice = "";
}



if (empty($getBrends) && empty($getCats) && empty($getPromotions) && empty($getPrice) && empty($getProizvod) && empty($getGender)) {
	if ($request->type == "Rastuće") {
		$sql = "SELECT * FROM `proizvodi` ORDER BY `cena` ASC";		
	} else if ($request->type == 'Opadajuće') {
		$sql = "SELECT * FROM `proizvodi` ORDER BY `cena` DESC";
	} else {
		$sql = "SELECT * FROM `proizvodi`";		
	}
} else {
	if (empty($getBrends)) {
		$getBrends = "IS NOT NULL";
	}
	if ($request->type == "Rastuće") {
		$sql = "SELECT * FROM `proizvodi` WHERE fk_brend_id $getBrends $getCats $getPromotions $getPrice $getProizvod $getGender ORDER BY `cena` ASC";	
	} else if ($request->type == "Opadajuće") {
		$sql = "SELECT * FROM `proizvodi` WHERE fk_brend_id $getBrends $getCats $getPromotions $getPrice $getProizvod $getGender ORDER BY `cena` DESC";	
	} else {
		$sql = "SELECT * FROM `proizvodi` WHERE fk_brend_id $getBrends $getCats $getPromotions $getPrice $getProizvod $getGender";	
	}
}


$result = $conn->query($sql);

$data = array();

if ($result) {
	if ($result->num_rows) {
		while($row = $result->fetch_object()) {
			$data[] = $row;
		}
	}
} else {	
	$data[] = null;
}


if (empty($data)) {
	if ($brends == "''" && $categories == "''" && $promotions == "''" && $getPrice == "''" && $getProizvod = "''" && $getGender = "''") {
		echo json_encode(array("deselect" => true));		
	} else {
		echo json_encode(array("empty" => true));
	}
} else {
	if (!is_null($data[0])) {
		for ($i = 0; $i < count($data); $i++) {

			$naslovna = mysqli_query($conn, "SELECT `slika` FROM `slike` WHERE `fk_proizvod_id` = ".$data[$i]->proizvod_id."");

			$slika = mysqli_fetch_assoc($naslovna);
			$data[$i]->naslovna_slika = $slika;			
		}
		echo json_encode($data);
	}
}

$conn->close();


?>

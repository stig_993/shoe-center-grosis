<?php
include_once '../mysql.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn, "utf8");

// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$type = $request->type;

if ($type == 'Opadajuće') {
	$sql = "SELECT * FROM `proizvodi` ORDER BY `cena` DESC";
} else {
	$sql = "SELECT * FROM `proizvodi` ORDER BY `cena` ASC";
}
$result = $conn->query($sql);

$data = array();

if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_object()) {
	$data[] = $row;
}
} else {
	$data[] = null;
}

for ($i = 0; $i < count($data); $i++) {

	$naslovna = mysqli_query($conn, "SELECT `slika` FROM `slike` WHERE `fk_proizvod_id` = ".$data[$i]->proizvod_id."" );

	$slika = mysqli_fetch_assoc($naslovna);

	$data[$i]->naslovna_slika = $slika;
}

if (is_null($data[0]->naslovna_slika) && is_null($data[0]->proizvod_id)) {
	$data[]->empty = true;
} else {
	echo json_encode($data);
}

$conn->close();


?>

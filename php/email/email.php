<?php
    
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $cartArray = $request->cart;

    $sum = $request->sum;

    $ime = $request->info->ime;
    $telefon = $request->info->telefon;
    $email = $request->info->email;
    $adresa = $request->info->adresa;
    $grad = $request->info->grad;
    $code = $request->info->code;


    $to = "shoecenter2017@gmail.com";
    $subject = "Website - narudzbina";

    $body = "<h2 style='text-align:center;width: 100%'>Narudzbina</h2>
        <p style='width:100%'><span style='font-weight:bold'>Ime i prezime:</span> $ime</p>
        <p style='width:100%'><span style='font-weight:bold'>Telefon:</span> $telefon</p>
        <p style='width:100%'><span style='font-weight:bold'>E-mail:</span> $email</p>
        <p style='width:100%'><span style='font-weight:bold'>Adresa:</span> $adresa</p>
        <p style='width:100%'><span style='font-weight:bold'>Grad:</span> $grad</p>
        <p style='width:100%'><span style='font-weight:bold'>Postanski broj:</span> $code</p>

        <table border='1' cellpadding='10'>
          <tr>
            <th>Naziv proizvoda</th>
            <th>Sifra proizvoda</th>
            <th>Kolicina</th>
            <th>Velicina</th>
            <th>Cena</th>
          </tr>
          ";
          for($i = 0; $i < count($cartArray); $i++) {
            $body .="<tr>";
            $body .="<td>" . $request->cart[$i]->naziv . "</td>";
            $body .="<td>" . $request->cart[$i]->sifra . "</td>";
            $body .="<td>" . $request->cart[$i]->kolicina . "</td>";
            $body .="<td>" . $request->cart[$i]->velicina . "</td>";
            $body .="<td>" . $request->cart[$i]->cena . "</td>";
            $body .="</tr>";
          }
        $body .= "</table>
        <h3>Ukupno: $request->sum RSD </h3>

        ";



    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers
    $headers .= "From: <grosis@website.com>" . "\r\n";

    mail($to,$subject,$body,$headers);
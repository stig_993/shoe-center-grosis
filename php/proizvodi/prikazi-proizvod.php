<?php 

include_once '../mysql.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn, "utf8");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$id = $request->id;

$sql = mysqli_query($conn, "SELECT * FROM `proizvodi` WHERE `proizvod_id` = '$id'" );

$proizvod = mysqli_fetch_assoc($sql);


$brend = mysqli_query($conn, "
	SELECT * FROM `brend` 
	WHERE `brend_id` = " . $proizvod['fk_brend_id'] . "" 
	);

$b = mysqli_fetch_assoc($brend);


$velicine = "SELECT broj from `proizvod-velicina` pv 
LEFT JOIN velicina v on pv.fk_velicina_id = v.velicina_id WHERE `fk_proizvod_id` = '$id'";

$result = $conn->query($velicine);

$data = array();

if ($result->num_rows > 0) {
	while($row = $result->fetch_object()) {
		$data[] = $row;
	}
} else {
	$data[] = null;
}


$sql360 = "SELECT * FROM `360slike` WHERE `fk_proizvod_id` = '$id'";
$result360 = $conn->query($sql360);

$data360 = array();

if ($result360->num_rows > 0) {
// output data of each row
while($row360 = $result360->fetch_object()) {
$data360[] = $row360;
}
} else {
$data360[] = null;
}


$proizvodArray = array('proizvod'=>$proizvod, 'brend' => $b, 'velicine' => $data, 'panSlike' => $data360);

echo json_encode($proizvodArray);

?>

<?php

include_once '../mysql.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


// Proizvod
$naziv = str_replace("'", "", $request->product->naziv);
$brend = $request->product->brend;
$sifra = $request->product->sifra;
$cena = str_replace(str_split(',.'), "", $request->product->cena);
$stara_cena = str_replace(str_split(',.'), "", $request->product->stara_cena);
$kratak_opis = addslashes($request->product->kratak_opis);
$duzi_opis = $request->product->duzi_opis;
// $duzi_opis = stripslashes($conn, $request->product->duzi_opis);
$dostupno = $request->product->dostupno;
$shop_strana = $request->product->shop_strana;
$home_strana = $request->product->home_strana;
$promocija = $request->product->promocija;
$kategorija = $request->product->kategorija;
$removedPanImages = $request->removePanImgs;
$mz = $request->product->mz;
$id = $request->id;


if (empty($shop_strana)) {
	$shop_strana = '0';
}
if (empty($dostupno)) {
	$dostupno = '0';
}
if (empty($home_strana)) {
	$home_strana = '0';
}
if (empty($kategorija)) {
	$kategorija = '0';
}
if (empty($brend)) {
	$brend = '0';
}

if (empty($promocija)) {
	$promocija = '0';
}



$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


$sql = "UPDATE `proizvodi` SET `naziv` = '$naziv', `sifra` = '$sifra', `cena` = '$cena', `stara_cena` = '$stara_cena', `kratak_opis` = '$kratak_opis', `duzi_opis` = '$duzi_opis', `dostupnost` = '$dostupno', `shop_strana` = '$shop_strana', `home_strana` = '$home_strana', `mz` = '$mz', `fk_brend_id` = '$brend', `fk_promocija_id` = '$promocija', `fk_kategorija_id` = '$kategorija' WHERE `proizvod_id` = '$id'";

$result = $conn->query($sql);

var_dump($sql);


for ($n = 0; $n < count($request->removedImages); $n++) {
	$sql2 = "DELETE FROM `slike` WHERE `slika_id` = ".$request->removedImages[$n]->id."";
	$result2 = $conn->query($sql2);
	// $filename = "../../uploads/proizvodi/". $request->removedImages[$n]->slika . "";
	// $compressedImage = "../../uploads/proizvodi/compressed/". $request->removedImages[$n]->slika . "";

	// unlink($filename);
	// unlink($compressedImage);
}

// Velicine

for($num = 0; $num < count($request->removedSize); $num++) {

	$fk_proizvod = $request->removedSize[$num]->proizvod_id;
	$fk_velicina = $request->removedSize[$num]->velicina_id;


	$deleteVelicine = "DELETE FROM `proizvod-velicina` WHERE `fk_proizvod_id` = $fk_proizvod AND `fk_velicina_id` = $fk_velicina";

	$result2 = $conn->query($deleteVelicine);
}


for($num2 = 0; $num2 < count($request->product->velicine); $num2++) {
	$insertVelicine = "INSERT INTO `proizvod-velicina` (`fk_proizvod_id`, `fk_velicina_id`) VALUES('{$id}', '{$request->product->velicine[$num2]}')";


	$result3 = $conn->query($insertVelicine);
}

// Slike

if (!empty($request->images->image)) {
	for ($i = 0; $i < count($request->images->image); $i++) {
		
		$filename = mt_rand().$request->images->image[$i]->filename;
		$images = base64_decode($request->images->image[$i]->base64);

		$file = file_put_contents("../../uploads/proizvodi/$filename", $images, FILE_APPEND | LOCK_EX);

		// if ($request->images->image[$i]->filetype == 'image/png') {
		// 	$img = imagecreatefrompng("../../uploads/proizvodi/$filename"); 
		// } else {
		// 	$img = imagecreatefromjpeg("../../uploads/proizvodi/$filename"); 
		// }

		// imagejpeg($img, "../../uploads/proizvodi/compressed/$filename", 50);

		$insertImages = "INSERT INTO `slike` (`slika`, `fk_proizvod_id`) VALUES('{$filename}', '{$id}')";

		$result4 = $conn->query($insertImages);
	}


// 	foreach ($request->images->image as $image) {

// 		if ($image->filetype == 'image/png') {
// 			$img = imagecreatefrompng("../../uploads/proizvodi/$filename"); 
// 		} else {
// 			$img = imagecreatefromjpeg("../../uploads/proizvodi/$filename"); 
// 		}
// 		imagejpeg($img, "../../uploads/proizvodi/compressed/$filename", 80);

// 		// list($width, $height) = getimagesize("../../uploads/proizvodi/$image->filename");

// 		// $newwidth = 800;
// 		// $newheight = ($height / $width) * $newwidth;

// 		// if ($width < $newwidth) {
// 		// 	imagejpeg($img, "../../uploads/proizvodi/compressed/$image->filename", 90);
// 		// } else {
// 		// 	$tmp = imagecreatetruecolor($newwidth, $newheight);
// 		// 	imagecopyresampled($tmp, $img, 0,0,0,0, $newwidth, $newheight, $width, $height);
// 		// 	imagejpeg($tmp, "../../uploads/proizvodi/compressed/$image->filename", 80);
// 		// }
// 	}
}

if ($removedPanImages) {
	$sqlPan = "DELETE FROM `360slike` WHERE fk_proizvod_id='$id'";
	$conn->query($sqlPan);
}

if (!empty($request->panorama->image)) {
	for ($i = 0; $i < count($request->panorama->image); $i++) {

		$panFilename = mt_rand().$request->panorama->image[$i]->filename;
		$panorama = base64_decode($request->panorama->image[$i]->base64);

		$file = file_put_contents("../../uploads/proizvodi/360/$panFilename", $panorama, FILE_APPEND | LOCK_EX);

		// if ($request->images->image[$i]->filetype == 'image/png') {
		// 	$img = imagecreatefrompng("../../uploads/proizvodi/$filename"); 
		// } else {
		// 	$img = imagecreatefromjpeg("../../uploads/proizvodi/$filename"); 
		// }

		// imagejpeg($img, "../../uploads/proizvodi/compressed/$filename", 50);

		$insertPanImages = "INSERT INTO `360slike` (`slika`, `fk_proizvod_id`) VALUES('{$panFilename}', '{$id}')";

		$result5 = $conn->query($insertPanImages);
	}
}

?>
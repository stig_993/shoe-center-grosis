<?php

include_once '../mysql.php';
// $servername = "localhost";
// $username = "root";
// $password = "";
// $dbname = "grosis";

$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


// Proizvod
$naziv = str_replace("'", "", $request->product->naziv);
$brend = $request->product->brend;
$kategorija = $request->product->kategorija;
$sifra = $request->product->sifra;
$cena = str_replace(str_split(',.'), "", $request->product->cena);
$stara_cena = str_replace(str_split(',.'), "", $request->product->stara_cena);
$kratak_opis = addslashes($request->product->kratak_opis);
// $duzi_opis = stripslashes($conn, $request->product->duzi_opis);
$duzi_opis = addslashes($request->product->duzi_opis);
$dostupno = $request->product->dostupno;
$shop_strana = $request->product->shop_strana;
$home_strana = $request->product->home_strana;
$promocija = $request->product->promocija;
$mz = $request->product->mz;

if (empty($shop_strana)) {
	$shop_strana = '0';
}
if (empty($dostupno)) {
	$dostupno = '0';
}
if (empty($home_strana)) {
	$home_strana = '0';
}
if (empty($promocija)) {
	$promocija = '0';
}
if (empty($stara_cena)) {
	$stara_cena = '0';
}

if (empty($kategorija)) {
	$kategorija = '0';
}
if (empty($brend)) {
	$brend = '0';
}


$datum = date('Y-m-d H:i:s');
$sql = "INSERT INTO `proizvodi` (`naziv`, `sifra`, `cena`,`stara_cena`,`kratak_opis`, `duzi_opis`, `dostupnost`, `shop_strana`, `home_strana`, `mz`, `datum`, `fk_brend_id`, `fk_promocija_id`, `fk_kategorija_id`) VALUES('{$naziv}', '{$sifra}', '{$cena}', '{$stara_cena}', '{$kratak_opis}', '{$duzi_opis}', '{$dostupno}', '{$shop_strana}', '{$home_strana}', '{$mz}', '{$datum}', '{$brend}', '{$promocija}', '{$kategorija}')";


$result = $conn->query($sql);

$getProduct = mysqli_query($conn, "SELECT * FROM `proizvodi` WHERE `naziv` = '$naziv'" );

$product = mysqli_fetch_assoc($getProduct);

// Velicine
for($n = 0; $n < count($request->product->velicine); $n++) {
	$insertVelicine = "INSERT INTO `proizvod-velicina` (`fk_proizvod_id`, `fk_velicina_id`) VALUES('{$product['proizvod_id']}', '{$request->product->velicine[$n]}')";

	$result2 = $conn->query($insertVelicine);
}

// Slike

if (empty($request->images->image)) {
	die();
}

for ($i = 0; $i < count($request->images->image); $i++) {
	
	$filename = mt_rand().$request->images->image[$i]->filename;
	$images = base64_decode($request->images->image[$i]->base64);

	$file = file_put_contents("../../uploads/proizvodi/$filename", $images, FILE_APPEND | LOCK_EX);

	// if ($request->images->image[$i]->filetype == 'image/png') {
	// 	$img = imagecreatefrompng("../../uploads/proizvodi/$filename"); 
	// } else {
	// 	$img = imagecreatefromjpeg("../../uploads/proizvodi/$filename"); 
	// }

	// imagejpeg($img,"../../uploads/proizvodi/compressed/$filename", 50);
	

	$insertImages = "INSERT INTO `slike` (`slika`, `fk_proizvod_id`) VALUES('{$filename}', '{$product['proizvod_id']}')";
	$result3 = $conn->query($insertImages);
}

// foreach ($request->images->image as $image) {

// 	if ($image->filetype == 'image/png') {
// 		$img = imagecreatefrompng("../../uploads/proizvodi/$filename"); 
// 	} else {
// 		$img = imagecreatefromjpeg("../../uploads/proizvodi/$filename"); 
// 	}
// 	imagejpeg($img, "../../uploads/proizvodi/compressed/$filename", 80);

// 	// list($width, $height) = getimagesize("../../uploads/proizvodi/$filename");

// 	// $newwidth = 800;
// 	// $newheight = ($height / $width) * $newwidth;

// 	// if ($width < $newwidth) {
// 	// 	imagejpeg($img, "../../uploads/proizvodi/compressed/$filename", 90);
// 	// } else {
// 	// 	$tmp = imagecreatetruecolor($newwidth, $newheight);
// 	// 	imagecopyresampled($tmp, $img, 0,0,0,0, $newwidth, $newheight, $width, $height);
// 	// }
// }

if (!empty($result->panorama->image)) {
	for ($i = 0; $i < count($request->panorama->image); $i++) {
	
		$panfilename = mt_rand().$request->panorama->image[$i]->filename;
		$panorama = base64_decode($request->panorama->image[$i]->base64);

		$file = file_put_contents("../../uploads/proizvodi/360/$panfilename", $panorama, FILE_APPEND | LOCK_EX);

		// if ($request->images->image[$i]->filetype == 'image/png') {
		// 	$img = imagecreatefrompng("../../uploads/proizvodi/$filename"); 
		// } else {
		// 	$img = imagecreatefromjpeg("../../uploads/proizvodi/$filename"); 
		// }

		// imagejpeg($img,"../../uploads/proizvodi/compressed/$filename", 50);
		

		$insertImages = "INSERT INTO `360slike` (`slika`, `fk_proizvod_id`) VALUES('{$panfilename}', '{$product['proizvod_id']}')";

		$result3 = $conn->query($insertImages);
	}
}


?>

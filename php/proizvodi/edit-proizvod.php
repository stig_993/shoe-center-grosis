<?php 

include_once '../mysql.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
mysqli_set_charset($conn, "utf8");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$id = $request->id;

$sql = mysqli_query($conn, "SELECT * FROM `proizvodi` WHERE `proizvod_id` = '$id'" );

$proizvod = mysqli_fetch_assoc($sql);

$brend = mysqli_query($conn, "
	SELECT * FROM `brend` 
	WHERE `brend_id` = " . $proizvod['fk_brend_id'] . "" 
	);

$b = mysqli_fetch_assoc($brend);

$s = "SELECT * FROM `slike` WHERE `fk_proizvod_id` = '$id'";
$result = $conn->query($s);

$slike = array();

if ($result->num_rows > 0) {
	while($row = $result->fetch_object()) {
		$slike[] = $row;
	}
} else {
	$slike[] = null;
}


$velicine = "SELECT * from `proizvod-velicina` pv 
LEFT JOIN velicina v on pv.fk_velicina_id = v.velicina_id WHERE `fk_proizvod_id` = '$id'";

$result2 = $conn->query($velicine);

$data = array();

if ($result2->num_rows > 0) {
	while($row = $result2->fetch_object()) {
		$data[] = $row;
	}
} else {
	$data[] = null;
}

$panSql = "SELECT * FROM `360slike` WHERE `fk_proizvod_id` = '$id'";
$resultPan = $conn->query($panSql);

$panSlike = array();

if ($resultPan->num_rows > 0) {
	while($rowPan = $resultPan->fetch_object()) {
		$panSlike[] = $rowPan;
	}
} else {
	$panSlike[] = null;
}


$proizvodArray = array('proizvod'=>$proizvod, 'brend'=>$b, 'slike'=>$slike, 'panSlike' => $panSlike, 'velicine'=>$data);

echo json_encode($proizvodArray);

?>

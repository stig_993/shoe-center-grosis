<?php
    

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $imeprezime = $request->ime;
    $email = $request->email;
    $msg = $request->poruka;
    
    $to = "info@shoecentergrosis.rs";
    $subject = "Website - poruka";

    $message = "
    <html>
    <head>
    <title>Grosis poruka</title>
    </head>
    <body>
        <div>
            <h3>Od: $imeprezime</h3>
            <h3>E-mail: $email</h3>
            <h3>Poruka: $msg</h3>
        </div>
    </body>
    </html>
    ";

    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers
    // $headers .= "From: <dentalacademy@website.com>" . "\r\n";
    $headers .= "From: $email" . "\r\n";

    mail($to,$subject,$message,$headers);
?>
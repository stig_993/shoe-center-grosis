<?php

include_once '../../mysql.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM `brend`";
$result = $conn->query($sql);

$data = array();

if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_object()) {
$data[] = $row;
}
} else {
$data[] = null;
}
$conn->close();

echo json_encode($data);

?>

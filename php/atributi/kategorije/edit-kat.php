<?php 

include_once '../../mysql.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$id = $request->id;

$sql = mysqli_query($conn, "SELECT * FROM `kategorija` WHERE `kat_id` = '$id'" );

$proizvod = mysqli_fetch_assoc($sql);

echo json_encode($proizvod);

?>
